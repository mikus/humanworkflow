﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core;
using ArticleWorkflow;
using CommonActivityLibrary;

namespace Client
{
    public class Client
    {
        private User user;
        private ArticleManager manager;

        public Client(int userId)
        {
            user = UsersRepository.Get(userId);
        }

        public void run()
        {
            using (manager = ArticleManager.Instance)
            {
                WorkflowInstance<Workflow> current = null;
                while (true)
                {
                    manager.LoadInstances();
                    Console.WriteLine();
                    Console.WriteLine("użytkownik {0}", user.Name);
                    Console.WriteLine();
                    Console.WriteLine("Workflows:");
                    foreach (var w in manager.Workflows.Values)
                        Console.WriteLine(w.Id);
                    Console.WriteLine();
                    Console.WriteLine("Current instance: {0}", (current != null ? current.Id.ToString() : "<none>"));
                    Console.WriteLine();
                    Console.WriteLine("Actions:");
                    Console.WriteLine("========");
                    if (current != null)
                    {
                        var trans = manager.PossibleTransitions(current.Id, user);
                        if (trans != null)
                            foreach (var t in trans)
                                Console.WriteLine(t);
                    }
                    Console.WriteLine("---------------------------------");
                    if (current != null)
                    {
                        Console.WriteLine("COMMENTS - display comments");
                        Console.WriteLine("---------------------------------");
                    }
                    Console.WriteLine("NEW - new workflow");
                    Console.WriteLine("CHANGE - chage current workflow");
                    Console.WriteLine("CLEAR - clear workflow list");
                    Console.WriteLine("EXIT - exit program");
                    var cmd = Console.ReadLine();
                    switch (cmd)
                    {
                        case "NEW":
                            manager.NewArticle(user);
                            break;
                        case "CHANGE":
                            var list = new List<WorkflowInstance<Workflow>>(manager.Workflows.Values);
                            for (var i = 0; i < list.Count; ++i)
                                Console.WriteLine("{0}: {1}", i, list.ElementAt(i).Id);
                            Console.WriteLine("Type Number");
                            var idx = Convert.ToInt32(Console.ReadLine());
                            if (idx >= 0 && idx < list.Count)
                            {
                                current = list.ElementAt(idx);
                                manager.Load(current.Id);
                            }
                            break;
                        case "CLEAR":
                            manager.Clear();
                            break;
                        case "EXIT":
                            Environment.Exit(0);
                            break;
                        case "COMMENTS":
                            if (current != null)
                                foreach (var c in manager.GetComments(current.Id))
                                    Console.WriteLine(c);
                            break;
                        case "":
                            break;
                        default:
                            Console.WriteLine("Comment:");
                            var comment = Console.ReadLine();
                            manager.ExecuteTransition(current.Id, cmd, user, comment);
                            break;
                    }
                }
            }
        }
    }
}
