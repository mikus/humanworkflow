﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Core
{
    [DataContract]
    public class User
    {
        [DataMember]
        public int Id { get; private set; }
        [DataMember]
        private string name;
        [DataMember]
        private List<Role> roles;

        public string Name { get { return name; } }
        public ReadOnlyCollection<Role> Roles { get { return roles.AsReadOnly(); } }

        public User(int Id, string Name, List<Role> Roles)
        {
            this.Id = Id;
            this.name = Name;
            this.roles = new List<Role>();
            this.roles.AddRange(Roles);
        }
    }
}
