﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core
{
    public class UsersRepository
    {
        private static Dictionary<int, User> users = new Dictionary<int,User> {
                                                  {1, new User(1, "Autor", new List<Role> { Role.Creator } ) },
                                                  {2, new User(2, "Redaktor", new List<Role> { Role.Redactor } ) },
                                                  {3, new User(3, "Publikator", new List<Role> { Role.Publisher } ) },
                                              };

        public static User Get(int id)
        {
            return users[id];
        }
    }
}
