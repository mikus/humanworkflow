﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core;
using System.Runtime.Serialization;

namespace ArticleWorkflow
{
    [DataContract]
    public class UserComment
    {
        [DataMember]
        public User User { get; set; }
        [DataMember]
        public string Comment { get; set; }
        [DataMember]
        public string Action { get; set; }

        public override string ToString()
        {
            return Action + " by " + User.Name + ": " + Comment;
        }
    }
}
