﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Xml;

namespace ArticleWorkflow
{
    public class CommentsManager
    {
        private static string filename = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\comments.xml";

        private static CommentsManager instance;
        public static CommentsManager Instance { get { if (instance == null) instance = new CommentsManager(); return instance; } }

        private IDictionary<Guid, List<UserComment>> comments;
        public IDictionary<Guid, List<UserComment>> Comments
        {
            get
            {
                Deserialize();
                return comments;
            }
        }

        private CommentsManager()
        {
            try
            {
                Deserialize();
            }
            catch (Exception e)
            {
                comments = new Dictionary<Guid, List<UserComment>>();
                Serialize();
            }
        }

        public static void AddComment(Guid guid, UserComment comment)
        {
            var instance = Instance;
            instance.Deserialize();
            List<UserComment> entry = null;
            if (!instance.Comments.TryGetValue(guid, out entry))
            {
                entry = new List<UserComment>();
                instance.Comments[guid] = entry;
            }
            entry.Add(comment);
            instance.Serialize();
        }

        public void Serialize()
        {
            var serializer = new DataContractSerializer(typeof(IDictionary<Guid, List<UserComment>>));
            using (var writer = new XmlTextWriter(filename, Encoding.Unicode))
            {
                writer.Formatting = Formatting.Indented;
                serializer.WriteObject(writer, comments);
                writer.Flush();
            }
        }

        public void Deserialize()
        {
            var serializer = new DataContractSerializer(typeof(IDictionary<Guid, List<UserComment>>));
            using (var reader = new XmlTextReader(filename))
            {
                comments = (IDictionary<Guid, List<UserComment>>)serializer.ReadObject(reader);
            }
        }
    }
}
