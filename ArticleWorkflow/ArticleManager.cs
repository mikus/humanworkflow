﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonActivityLibrary;
using Core;

namespace ArticleWorkflow
{
    public class ArticleManager : HumanWorkflowHostManager<Workflow>
    {
        private static ArticleManager instance;
        public static ArticleManager Instance { get { if (instance == null) instance = new ArticleManager(); return instance; } }

        private ArticleManager() : base(Config.Instance) { }

        public void NewArticle(User user)
        {
            this.New(new Dictionary<string, object> { {"Author", user} });
        }

        public List<UserComment> GetComments(Guid id)
        {
            List<UserComment> results;
            if (!CommentsManager.Instance.Comments.TryGetValue(id, out results))
                results = new List<UserComment>();
            return results;
        }
    }
}
