﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonActivityLibrary;
using System.Runtime.DurableInstancing;
using System.Activities.DurableInstancing;
using System.Activities;
using System.Diagnostics;
using Microsoft.Activities.Extensions.DurableInstancing;
using Microsoft.Activities.Extensions.Tracking;

namespace ArticleWorkflow
{
    public class Config : IWorkflowConfig
    {
        #region Constants

        private const string ConnString =
            @"Data Source=.\SQLEXPRESS;Initial Catalog=" + SampleInstanceStore
            + ";Integrated Security=True;Asynchronous Processing=True";

        private const string SampleInstanceStore = "SampleInstanceStore";

        #endregion

        private SqlWorkflowInstanceStore instanceStore;

        private static Config instance;
        public static Config Instance { get { if (instance == null) instance = new Config(); return instance; } }

        private Config()
        {
            this.InitializeInstanceStore();
        }

        public string ConnectionString
        {
            get { return ConnString; }
        }

        public InstanceStore InstanceStore
        {
            get { return instanceStore; }
        }

        public string DatabaseName
        {
            get { return SampleInstanceStore; }
        }

        public PersistableIdleAction PersistableIdle(System.Activities.WorkflowApplicationIdleEventArgs arg)
        {
            Console.WriteLine("Workflow {0} is persisting", arg.InstanceId);
            return PersistableIdleAction.Unload;
        }

        public UnhandledExceptionAction OnUnhandledException(System.Activities.WorkflowApplicationUnhandledExceptionEventArgs args)
        {
            Console.WriteLine(args.UnhandledException.Message);

            return UnhandledExceptionAction.Abort;
        }

        public void InitializeInstanceStore()
        {
            Trace.WriteLine("Verifying instance store " + this.DatabaseName);
            if (!SqlWorkflowInstanceStoreManager.InstanceStoreExists(this.DatabaseName, this.ConnectionString))
            {
                Trace.WriteLine("Creating instance store " + this.DatabaseName);
                SqlWorkflowInstanceStoreManager.CreateInstanceStore(this.DatabaseName, this.ConnectionString);
            }

            this.instanceStore = new SqlWorkflowInstanceStore(this.ConnectionString);

            StateMachineStateTracker.Promote(this.instanceStore);
            HumanDataTracker.Promote(this.instanceStore);

            var createWorkflowOwnerCommand = new CreateWorkflowOwnerCommand();
            var handle = this.instanceStore.CreateInstanceHandle();

            try
            {
                this.instanceStore.BeginExecute(
                    handle, createWorkflowOwnerCommand, TimeSpan.FromSeconds(30), this.OnInstanceStoreEndExecute, null);
            }
            catch (InstancePersistenceException persistenceException)
            {
                Console.WriteLine("An error has occured setting up the InstanceStore: {0}", persistenceException);
            }
        }

        private void OnInstanceStoreEndExecute(IAsyncResult ar)
        {
            InstanceView ownerView;

            try
            {
                ownerView = this.instanceStore.EndExecute(ar);
            }
            catch (InstancePersistenceException persistenceException)
            {
                Console.WriteLine("An error has occured setting up the InstanceStore: {0}", persistenceException);
                return;
            }

            this.instanceStore.DefaultInstanceOwner = ownerView.InstanceOwner;
        }
    }
}
