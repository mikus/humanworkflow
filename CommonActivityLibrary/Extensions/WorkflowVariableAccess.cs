﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Workflow.ComponentModel;

namespace CommonActivityLibrary
{
    public static class WorkflowVariableAccess
    {
        public static object GetValueOfWorkflowVariable(this Activity activity, string valueName)
        {
            object value = null;
            if (activity != null)
            {
                try
                {
                    ActivityBind workflowActivityBind = new ActivityBind();
                    workflowActivityBind.Name = activity.Name;
                    workflowActivityBind.Path = valueName;
                    value = workflowActivityBind.GetRuntimeValue(activity);
                }
                catch
                { }
                if (value == null)
                    value = GetValueOfWorkflowVariable(activity.Parent, valueName);
            }
            return value;
        }

        public static void SetValueOfWorkflowVariable(this Activity activity, string valueName, object value)
        {
            if (activity != null)
            {
                try
                {
                    ActivityBind workflowActivityBind = new ActivityBind();
                    workflowActivityBind.Name = activity.Name;
                    workflowActivityBind.Path = valueName;
                    workflowActivityBind.SetRuntimeValue(activity, value);
                }
                catch
                { }
                SetValueOfWorkflowVariable(activity.Parent, valueName, value);
            }
        }
    }
}
