﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Microsoft.Activities.Extensions.Tracking;
using System.Diagnostics.Contracts;
using System.Diagnostics;
using System.Runtime.DurableInstancing;

namespace CommonActivityLibrary
{
    public class WorkflowHostManager<TWorkflow> : IWorkflowObserver, IDisposable where TWorkflow : Activity, new()
    {
        #region Fields

        private readonly IWorkflowConfig config;
        private HashSet<IWorkflowObserver> observers;

        #endregion

        #region Constructors and Destructors

        public WorkflowHostManager(IWorkflowConfig config)
        {
            this.config = config;
            this.observers = new HashSet<IWorkflowObserver>();
            this.Workflows = new Dictionary<Guid,WorkflowInstance<TWorkflow>>();
        }

        #endregion

        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Properties

        public Dictionary<Guid,WorkflowInstance<TWorkflow>> Workflows { get; private set; }

        #endregion

        #region Public Methods and Operators

        public void Dispose()
        {
            foreach (var workflowInstance in this.Workflows.Values.Where(w => w != null && w.Host != null))
            {
                Debug.Assert(workflowInstance != null, "workflowInstance != null");
                Debug.Assert(workflowInstance.Host != null, "workflowInstance.Host != null");
                workflowInstance.Host.Unload();
            }
        }

        public bool Load(Guid id)
        {
            WorkflowInstance<TWorkflow> instance = null;
            Workflows.TryGetValue(id, out instance);
            if (instance != null)
            {
                try
                {
                    instance.Load();
                    this.NotifyChanged("Workflows");
                    instance.Run();
                    return true;
                }
                catch (InstancePersistenceException persistenceException)
                {
                    Trace.WriteLine(persistenceException.Message);
                    return false;
                }
            }

            return false;
        }

        public void OnComplete(WorkflowApplicationCompletedEventArgs args)
        {
            Debug.Assert(this.Workflows != null, "Workflows is null");
            this.Workflows.Remove(args.InstanceId);
            Console.WriteLine("Workflow {0} has been completed", args.InstanceId);
        }

        public void Open(Guid instanceId)
        {
            Debug.Assert(this.config != null, "config is null");
            var sm = StateMachineStateTracker.LoadInstance(
                instanceId, WorkflowInstance<TWorkflow>.WorkflowDefinition, this.config.ConnectionString);
            if (sm == null)
            {
                return;
            }
            var hd = HumanDataTracker.LoadInstance(instanceId, this.config.ConnectionString);
            if (hd == null)
            {
                return;
            }

            CreateNewInstance(this.config, sm, hd);
            
            this.NotifyChanged("Workflows");
        }

        public void Unload(Guid id)
        {
            WorkflowInstance<TWorkflow> instance = null;
            Workflows.TryGetValue(id, out instance);
            if (instance != null)
            {
                instance.Unload();
                this.NotifyChanged("Workflows");
            }
        }

        public void AddObserver(IWorkflowObserver observer)
        {
            if (!observers.Contains(observer))
            {
                observers.Add(observer);
            }
        }

        public void RemoveObserver(IWorkflowObserver observer)
        {
            if (observers.Contains(observer))
            {
                observers.Remove(observer);
            }
        }

        #endregion

        #region Methods

        public void LoadInstances()
        {
            var instancesSM = StateMachineStateTracker.LoadInstances(WorkflowInstance<TWorkflow>.WorkflowDefinition, this.config.ConnectionString);
            var instancesHD = HumanDataTracker.LoadInstances(this.config.ConnectionString);
            if (instancesSM != null && instancesHD != null)
            {
                foreach (var stateMachineStateTracker in instancesSM)
                {
                    var humanDataTracker = instancesHD.Where(i => i.InstanceId == stateMachineStateTracker.InstanceId).Last();
                    CreateNewInstance(this.config, stateMachineStateTracker, humanDataTracker);
                }

                Debug.Assert(this.Workflows != null, "this.Workflows  is null");
                Trace.WriteLine(
                    string.Format(
                        "Loaded {0} instance{1} from instance store", 
                        this.Workflows.Count, 
                        this.Workflows.Count != 1 ? "s" : string.Empty));
            }
            else
            {
                Trace.WriteLine("Error loading instances - be sure to create the SampleInstanceStore database first");
            }
        }

        public void Clear()
        {
            foreach (Guid id in this.Workflows.Keys.ToArray())
            {
                this.Unload(id);
                this.Workflows.Remove(id);
            }
        }

        public Guid New(IDictionary<string, object> inputs = null)
        {
            var instance = CreateNewInstance(this.config, null, null, false);
            instance.New(inputs);
            instance.Run();
            this.Workflows[instance.Id] = instance;
            return instance.Id;
        }

        private void NotifyChanged(string propertyName)
        {
            var onPropertyChanged = this.PropertyChanged;
            if (onPropertyChanged != null)
            {
                onPropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private WorkflowInstance<TWorkflow> CreateNewInstance(IWorkflowConfig config, StateMachineStateTracker stateMachineStateTracker, HumanDataTracker humanDataTracker, bool addToCollection = true)
        {
            var instance = new WorkflowInstance<TWorkflow>(config, stateMachineStateTracker, humanDataTracker);
            instance.AddObserver(this);
            foreach (var observer in observers)
                instance.AddObserver(observer);
            if (addToCollection)
            {
                Debug.Assert(this.Workflows != null, "this.Workflows is null");
                this.Workflows[instance.Id] = instance;
            }
            return instance;
        }

        #endregion

        public void OnAbort(WorkflowApplicationAbortedEventArgs args)
        {
            Console.WriteLine("Workflow {0} is idle", args.InstanceId);
        }

        public void OnIdle(WorkflowApplicationIdleEventArgs args) 
        {
            Console.WriteLine("Workflow {0} is idle", args.InstanceId);
        }

        public void OnUnload(WorkflowApplicationEventArgs obj) 
        {
            Console.WriteLine("Workflow {0} has been unloaded", obj.InstanceId);
        }

        public void WriteException(Exception exception, string error)
        {
            Console.WriteLine(error);
        }

        void IWorkflowObserver.NotifyChanged(string property) { }
    }
}
