﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using Core;

namespace CommonActivityLibrary
{
    public class HumanWorkflowHostManager<TWorkflow> : WorkflowHostManager<TWorkflow> where TWorkflow : Activity, new()
    {
        public HumanWorkflowHostManager(IWorkflowConfig config) : base(config) { }

        public IEnumerable<string> PossibleTransitions(Guid id, User user)
        {
            WorkflowInstance<TWorkflow> instance = null;
            this.Workflows.TryGetValue(id, out instance);
            if (instance == null)
                return null;
            var humanTracker = instance.HumanDataTracker;
            var allTransitions = instance.StateTracker.Transitions;
            if (allTransitions == null)
                return null;
            var allTransitionNames = allTransitions.Select(t => t.DisplayName);
            var transitions = new HashSet<string>();
            foreach (var t in allTransitionNames)
            {
                if (humanTracker.AssignedUsers.ContainsKey(t)) {
                    var allowedUsers = humanTracker.AssignedUsers[t];
                    if (allowedUsers != null && allowedUsers.Count() > 0)
                        if (allowedUsers.Any(u => u == user.Id))
                        {
                            transitions.Add(t);
                            continue;
                        }
                }
                if (humanTracker.AssignedRoles.ContainsKey(t))
                {
                    var allowedRoles = humanTracker.AssignedRoles[t];
                    if (allowedRoles != null && allowedRoles.Count() > 0)
                        if (allowedRoles.Any(r => user.Roles.Contains(r)))
                            transitions.Add(t);
                }
            }
            return transitions;
        }

        public void ExecuteTransition(Guid id, string name, User user, object Data)
        {
            WorkflowInstance<TWorkflow> instance = null;
            this.Workflows.TryGetValue(id, out instance);
            if (instance == null)
                return;
            instance.ResumeBookmark(name, new UserTransitionResult(user, Data));
        }
    }
}
