﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;

namespace CommonActivityLibrary
{
    public interface IWorkflowObserver
    {
        #region Public Methods and Operators

        void OnAbort(WorkflowApplicationAbortedEventArgs args);

        void OnComplete(WorkflowApplicationCompletedEventArgs args);

        void OnIdle(WorkflowApplicationIdleEventArgs args);

        void OnUnload(WorkflowApplicationEventArgs obj);

        void WriteException(Exception exception, string error);

        #endregion

        void NotifyChanged(string property);
    }
}
