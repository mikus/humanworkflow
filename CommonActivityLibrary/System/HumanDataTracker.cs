﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities.Tracking;
using Core;
using System.Diagnostics.Contracts;
using System.Data.SqlClient;
using System.Activities.DurableInstancing;
using System.Xml.Linq;

namespace CommonActivityLibrary
{
    public class HumanDataTracker : TrackingParticipant
    {
        #region Constants

        private const string FindExistingInstanceSql =
            @"SELECT InstanceId, Value1
FROM [System.Activities.DurableInstancing].[InstancePromotedProperties]
WHERE PromotionName = @PromotionName 
AND InstanceId = @InstanceId;";

        private const string FindExistingInstancesSql =
            @"SELECT InstanceId, Value1      
FROM [System.Activities.DurableInstancing].[InstancePromotedProperties]
WHERE PromotionName = @PromotionName;";

        #endregion

        public Dictionary<string, IEnumerable<int>> AssignedUsers {get; set;}
        public Dictionary<string, IEnumerable<Role>> AssignedRoles {get; set;}

        public Guid InstanceId { get; internal set; }

        public HumanDataTracker()
        {
            AssignedUsers = new Dictionary<string, IEnumerable<int>>();
            AssignedRoles = new Dictionary<string, IEnumerable<Role>>();

            var all = "*";
            TrackingProfile = new TrackingProfile()
                {
                    Name = "CustomTrackingProfile",
                    Queries = 
                    {
                        new CustomTrackingQuery() 
                        {
                         Name = all,
                         ActivityName = all
                        }   
                    }
                };
        }

        protected override void Track(TrackingRecord record, TimeSpan timeout)
        {
            CustomTrackingRecord customTrackingRecord = record as CustomTrackingRecord;

            if ((customTrackingRecord != null) && (customTrackingRecord.Data.Count > 0))
            {
                if (customTrackingRecord.Name.Equals("UserTransition"))
                {
                    var bookmark = (string)customTrackingRecord.Data["Bookmark"];
                    var users = (IEnumerable<int>)customTrackingRecord.Data["AssignedUsers"];
                    var roles = (IEnumerable<Role>)customTrackingRecord.Data["AssignedRoles"];
                    AssignedUsers[bookmark] = users;
                    AssignedRoles[bookmark] = roles;
                }
            }
        }

        public static HumanDataTracker LoadInstance(Guid instanceId, string connectionString)
        {
            Contract.Requires(instanceId != null);
            if (instanceId == null)
            {
                throw new ArgumentNullException("instanceId");
            }

            Contract.Requires(!string.IsNullOrWhiteSpace(connectionString));
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                throw new ArgumentNullException("connectionString");
            }

            using (var connection = new SqlConnection(connectionString))
            {
                var command = new SqlCommand(FindExistingInstanceSql, connection);
                command.Parameters.AddWithValue("@PromotionName", HumanDataTrackerPersistenceProvider.HumanDataTrackerName);
                command.Parameters.AddWithValue("@InstanceId", instanceId);
                connection.Open();
                var dataReader = command.ExecuteReader();
                return dataReader.Read()
                           ? HumanDataTrackerPersistenceProvider.Parse(dataReader.GetGuid(0), dataReader.GetString(1))
                           : null;
            }
        }

        public static ICollection<HumanDataTracker> LoadInstances(string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                var command = new SqlCommand(FindExistingInstancesSql, connection);
                command.Parameters.AddWithValue("@PromotionName", HumanDataTrackerPersistenceProvider.HumanDataTrackerName);
                connection.Open();
                var list = new List<HumanDataTracker>();
                var dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    list.Add(HumanDataTrackerPersistenceProvider.Parse(dataReader.GetGuid(0), dataReader.GetString(1)));
                }

                dataReader.Close();
                return list;
            }
        }

        public static void Promote(SqlWorkflowInstanceStore instanceStore)
        {
            HumanDataTrackerPersistenceProvider.Promote(instanceStore);
        }
    }
}
