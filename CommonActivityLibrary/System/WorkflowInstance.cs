﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Activities;
using Microsoft.Activities.Extensions.Tracking;
using System.Diagnostics;
using System.Diagnostics.Contracts;

namespace CommonActivityLibrary
{
    public class WorkflowInstance<TWorkflow> : INotifyPropertyChanged where TWorkflow : Activity, new()
    {
        #region Static Fields

        internal static readonly TWorkflow WorkflowDefinition = new TWorkflow();

        #endregion

        #region Fields

        private readonly IWorkflowConfig config;
        private readonly HashSet<IWorkflowObserver> observers;

        private bool isLoaded;

        #endregion

        #region Constructors and Destructors

        public WorkflowInstance(IWorkflowConfig config, StateMachineStateTracker stateMachineStateTracker = null, HumanDataTracker humanDataTracker = null)
        {
            this.config = config;
            this.observers = new HashSet<IWorkflowObserver>();
            this.StateTracker = stateMachineStateTracker ?? new StateMachineStateTracker(WorkflowDefinition);
            this.HumanDataTracker = humanDataTracker ?? new HumanDataTracker();
            this.DefaultPersistableIdleAction = PersistableIdleAction.Persist;
        }

        #endregion

        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Properties

        public WorkflowApplication Host { get; set; }
        public PersistableIdleAction DefaultPersistableIdleAction { get; set; }

        public Guid Id
        {
            get
            {
                return this.Host == null ? this.StateTracker.InstanceId : this.Host.Id;
            }
        }

        public bool IsLoaded
        {
            get
            {
                return this.isLoaded;
            }

            set
            {
                this.isLoaded = value;
                this.NotifyChanged("IsLoaded");
            }
        }

        public StateMachineStateTracker StateTracker { get; private set; }
        
        public HumanDataTracker HumanDataTracker { get; private set; }

        #endregion

        #region Public Methods and Operators

        public void AddObserver(IWorkflowObserver observer)
        {
            if (!observers.Contains(observer))
            {
                observers.Add(observer);
                ConnectObserver(observer);
            }
        }

        public void RemoveObserver(IWorkflowObserver observer)
        {
            if (observers.Contains(observer))
            {
                DisconnectObserver(observer);
                observers.Remove(observer);
            }
        }

        public void Load(bool useDefaultPersistableIdleAction = false)
        {
            this.CreateWorkflowApplication(null, useDefaultPersistableIdleAction);
            this.Host.Load(this.StateTracker.InstanceId);
            this.IsLoaded = true;
        }

        public void New(IDictionary<string,object> inputs = null)
        {
            this.CreateWorkflowApplication(inputs);
            this.IsLoaded = true;
        }

        public void Run()
        {
            this.Host.Run();
        }

        public void Unload()
        {
            try
            {
                if (this.IsLoaded)
                {
                    this.Host.Unload();
                    this.IsLoaded = false;
                }
            }
            catch (Exception exception)
            {
                Trace.WriteLine(exception.Message);
            }
        }

        public void ResumeBookmark(string bookmark, object value)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(bookmark));
            if (string.IsNullOrWhiteSpace(bookmark))
            {
                throw new ArgumentNullException("bookmark");
            }

            if (!this.IsLoaded)
            {
                this.Load(true);
                this.Run();
            }

            var result = this.Host.ResumeBookmark(bookmark, value);
            Console.WriteLine("Result of bookmark '{0}' resume: {1}", bookmark, result);
            
            this.Host.PersistableIdle = this.config.PersistableIdle;
        }

        #endregion

        #region Methods

        private void CreateWorkflowApplication(IDictionary<string,object> inputs = null, bool useDefaultPersistableIdleAction = false)
        {
            if (inputs == null)
                this.Host = new WorkflowApplication(WorkflowDefinition);
            else
                this.Host = new WorkflowApplication(WorkflowDefinition, inputs);
            
            this.Host.InstanceStore = this.config.InstanceStore;
            this.Host.OnUnhandledException = this.config.OnUnhandledException;
            if (useDefaultPersistableIdleAction)
                this.Host.PersistableIdle = (args) =>this.DefaultPersistableIdleAction;
            else
                this.Host.PersistableIdle = this.config.PersistableIdle;
            
            foreach (var observer in observers)
                ConnectObserver(observer);

            this.Host.Extensions.Add(this.StateTracker);
            this.Host.Extensions.Add(this.HumanDataTracker);

            // Setup the persistence participant
            this.Host.Extensions.Add(new StateTrackerPersistenceProvider(this.StateTracker));
            this.Host.Extensions.Add(new HumanDataTrackerPersistenceProvider(this.HumanDataTracker));
        }

        private void ConnectObserver(IWorkflowObserver observer)
        {
            if (Host == null)
                return;
            Host.Idle += observer.OnIdle;
            Host.Aborted += observer.OnAbort;
            Host.Completed += observer.OnComplete;
            Host.Unloaded += observer.OnUnload;
        }

        private void DisconnectObserver(IWorkflowObserver observer)
        {
            if (Host == null)
                return;
            Host.Idle -= observer.OnIdle;
            Host.Aborted -= observer.OnAbort;
            Host.Completed -= observer.OnComplete;
            Host.Unloaded -= observer.OnUnload;
        }

        private void NotifyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
