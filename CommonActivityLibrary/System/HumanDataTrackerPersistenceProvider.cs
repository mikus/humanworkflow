﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities.Persistence;
using System.Xml.Linq;
using System.Diagnostics.Contracts;
using System.Activities.DurableInstancing;
using System.Activities;
using System.Diagnostics;
using Core;

namespace CommonActivityLibrary
{
    public class HumanDataTrackerPersistenceProvider : PersistenceParticipant
    {
        # region Static Fields
        
        public static readonly XNamespace Xns = XNamespace.Get("http://evcon.pl/extensions/persistence");
 
        private static readonly XName AssignedUsersX = XName.Get("AssignedUsers", string.Empty);
        private static readonly XName AssignedRolesX = XName.Get("AssignedRoles", string.Empty);
        private static readonly XName BookmarkX = XName.Get("Bookmark", string.Empty);
        private static readonly XName UserX = XName.Get("User", string.Empty);
        private static readonly XName RoleX = XName.Get("Role", string.Empty);
 
        #endregion
 
        #region Fields
 
        private readonly HumanDataTracker tracker;
 
        #endregion
 
        #region Constructors and Destructors
 
        public HumanDataTrackerPersistenceProvider(HumanDataTracker tracker)
        {
            Contract.Requires(tracker != null);
            if (tracker == null)
            {
                throw new ArgumentNullException("tracker");
            }
 
            this.tracker = tracker;
        }

        public HumanDataTrackerPersistenceProvider(HumanDataTracker tracker, SqlWorkflowInstanceStore instanceStore) : this(tracker)
        {
            Contract.Requires(instanceStore != null);
            if (instanceStore == null)
            {
                throw new ArgumentNullException("instanceStore");
            }
 
            Promote(instanceStore);
        }
 
        #endregion
 
        #region Public Properties

        public static string HumanDataTrackerName
        {
            get
            {
                return "HumanDataTracker";
            }
        }
 
        public static XName HumanDataTrackerXName
        {
            get
            {
                return Xns.GetName(HumanDataTrackerName);
            }
        }
 
        #endregion
 
        #region Public Methods and Operators
 
        public static void Promote(SqlWorkflowInstanceStore instanceStore)
        {
            Contract.Requires(instanceStore != null);
            if (instanceStore == null)
            {
                throw new ArgumentNullException("instanceStore");
            }
 
            instanceStore.Promote(
                HumanDataTrackerName, new List<XName> { HumanDataTrackerXName }, null);
        }
 
        #endregion
 
        #region Methods

        internal static HumanDataTracker Parse(Guid id, string xml)
        {
            var hd = new HumanDataTracker() { InstanceId = id };
            var doc = XDocument.Parse(xml);
            if (doc.Root != null)
            {
                hd.AssignedUsers = CreateBookmarkedDictionary<int>(doc.Root.Descendants("AssignedUsers").First(), UserX, (x) => Convert.ToInt32(x));
                hd.AssignedRoles = CreateBookmarkedDictionary<Role>(doc.Root.Descendants("AssignedRoles").First(), RoleX, (x) => (Role)Enum.Parse(typeof(Role), x));
            }
 
            return hd;
        }

        private static Dictionary<string, IEnumerable<T>> CreateBookmarkedDictionary<T>(XElement xElement, XName innerName, Func<string,T> converter)
        {
            var entryName = xElement.Name + "Entry";
            var dict = new Dictionary<string, IEnumerable<T>>();
            foreach (var entry in xElement.Descendants(entryName))
            {
                var key = entry.Attribute(BookmarkX).Value;
                var val = entry.Descendants(innerName).Count() > 0 
                        ? (from i in entry.Descendants(innerName) select converter(i.Value)).ToArray()
                        : null;
                dict[key] = val;
            }
            return dict;
        }
 
        protected override void CollectValues(
            out IDictionary<XName, object> readWriteValues, out IDictionary<XName, object> writeOnlyValues)
        {
            readWriteValues = null;
            writeOnlyValues = new Dictionary<XName, object>();
            var root = new XElement("HumanData");

            var usersElement = new XElement("AssignedUsers");
            foreach (var entryInfo in this.tracker.AssignedUsers)
            {
                var entryElement = new XElement("AssignedUsersEntry");
                entryElement.SetAttributeValue(BookmarkX, entryInfo.Key);
                if (entryInfo.Value != null)
                {
                    entryElement.Add(
                        from user in entryInfo.Value select new XElement(UserX, user)
                    );
                }
                usersElement.Add(entryElement);
            }
            root.Add(usersElement);

            var rolesElement = new XElement("AssignedRoles");
            foreach (var entryInfo in this.tracker.AssignedRoles)
            {
                var entryElement = new XElement("AssignedRolesEntry");
                entryElement.SetAttributeValue(BookmarkX, entryInfo.Key);
                if (entryInfo.Value != null)
                {
                    entryElement.Add(
                    from role in entryInfo.Value select new XElement(RoleX, Enum.GetName(typeof(Role), role))
                    );
                }
                rolesElement.Add(entryElement);
            }
            root.Add(rolesElement);
             
            writeOnlyValues.Add(HumanDataTrackerXName, root.ToString());
            Trace.WriteLine("HumanDataTracker Writing XML to Promoted Property " + HumanDataTrackerName);
            Trace.WriteLine(root.ToString());
        }
 
        #endregion
    }
}
