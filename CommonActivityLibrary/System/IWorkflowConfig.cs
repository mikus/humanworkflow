﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.DurableInstancing;
using System.Activities;

namespace CommonActivityLibrary
{
    public interface IWorkflowConfig
    {
        #region Public Properties

        string ConnectionString { get; }

        InstanceStore InstanceStore { get; }

        string DatabaseName { get; }

        #endregion

        #region Public Methods and Operators

        PersistableIdleAction PersistableIdle(WorkflowApplicationIdleEventArgs arg);

        UnhandledExceptionAction OnUnhandledException(WorkflowApplicationUnhandledExceptionEventArgs args);

        #endregion
    }
}
