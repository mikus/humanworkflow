﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core;

namespace CommonActivityLibrary
{
    public class UserTransitionResult
    {
        public User Invoker { get; set; }

        public object Data { get; set; }

        public DateTime Timestamp { get; set; }


        public UserTransitionResult()
        {
            this.Timestamp = DateTime.Now;
        }

        public UserTransitionResult(User Invoker, object Data) : this()
        {
            this.Invoker = Invoker;
            this.Data = Data;
        }
    }
}
