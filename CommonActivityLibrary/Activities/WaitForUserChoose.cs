﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using Core;

namespace CommonActivityLibrary
{

    public sealed class WaitForUserChoose : WaitForBookmark<UserChooseResult>
    {
        #region Properties

        [RequiredArgument]
        public InArgument<IEnumerable<string>> Options { get; set; }
        
        public InArgument<IEnumerable<User>> AssignedUsers { get; set; }

        public InArgument<IEnumerable<Role>> AssignedRoles { get; set; }

        private InArgument<bool> validatePermissions = true;
        public InArgument<bool> ValidatePermissions { get { return validatePermissions; } set { validatePermissions = value; } }

        #endregion

        #region Methods

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            base.CacheMetadata(metadata);
        }

        protected override void EndExecute(NativeActivityContext context, Bookmark bookmark, object value)
        {
            base.EndExecute(context, bookmark, value);
            UserChooseResult result = value as UserChooseResult;
            if (!this.Options.Get(context).Contains(result.ChosenOption))
                throw new InvalidOperationException("Option '" + result.ChosenOption + "' chosen outer of allowed options");
            if (this.ValidatePermissions.Get(context))
            {
                if (!AssignedUsers.Get(context).Contains(result.Invoker) &&
                    !result.Invoker.Roles.Any(role => AssignedRoles.Get(context).Contains(role)))
                    throw new InvalidOperationException("Invoker has no permission to make a choose");
            }
        }

        #endregion
    }
}
