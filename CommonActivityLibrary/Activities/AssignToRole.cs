﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using Core;

namespace CommonActivityLibrary
{

    public sealed class AssignToRole : CodeActivity
    {
        [RequiredArgument]
        public InArgument<Role> Role { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            Role role = context.GetValue(this.Role);
            Console.WriteLine("Assigning current step to role {0}", role);
        }
    }
}
