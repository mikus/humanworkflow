﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using Core;
using System.Activities.Tracking;

namespace CommonActivityLibrary
{

    public class WaitForUserTransition : WaitForBookmark<UserTransitionResult>
    {
        #region Properties

        public InArgument<IEnumerable<int>> AssignedUsers { get; set; }

        public InArgument<IEnumerable<Role>> AssignedRoles { get; set; }

        private InArgument<bool> validatePermissions = true;
        public InArgument<bool> ValidatePermissions { get { return validatePermissions; } set { validatePermissions = value; } }

        #endregion

        #region Methods

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            base.CacheMetadata(metadata);
        }

        protected override void Execute(NativeActivityContext context)
        {
            base.Execute(context);
            CustomTrackingRecord customRecord = new CustomTrackingRecord("UserTransition")
            {
                Data = 
                        {
                            {"Bookmark", this.BookmarkName.Get(context)},
                            {"AssignedUsers", this.AssignedUsers.Get(context)},
                            {"AssignedRoles", this.AssignedRoles.Get(context)}
                        }
            };
            context.Track(customRecord);
        }

        protected override void EndExecute(NativeActivityContext context, Bookmark bookmark, object value)
        {
            base.EndExecute(context, bookmark, value);
            if (this.ValidatePermissions.Get(context))
            {
                Validation(context, value as UserTransitionResult);
            }
        }

        protected void Validation(NativeActivityContext context, UserTransitionResult result)
        {
            var assignedUsers = AssignedUsers.Get(context);
            var assignedRoles = AssignedRoles.Get(context);
            
            var valid = false;
            if (assignedUsers == null && assignedRoles == null)
                valid = true;
            else
            {
                if (assignedUsers != null && assignedUsers.Contains(result.Invoker.Id))
                    valid = true;
                if (assignedRoles != null && result.Invoker.Roles.Any(role => assignedRoles.Contains(role)))
                    valid = true;
            }
            if (!valid)
                throw new InvalidOperationException("Invoker has no permission to make a choose");
        }

        #endregion
    }
}

