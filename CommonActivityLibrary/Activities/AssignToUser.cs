﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using Core;

namespace CommonActivityLibrary
{

    public sealed class AssignToUser : CodeActivity
    {
        [RequiredArgument]
        public InArgument<User> User { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            User user = context.GetValue(this.User);
            Console.WriteLine("Assigning current step to user {0}", user);
        }
    }
}
