﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;

namespace CommonActivityLibrary
{
    public class WaitForBookmark : NativeActivity
    {
        #region Constants and Fields

        private BookmarkCallback bookmarkCallback;

        #endregion

        #region Public Properties

        [RequiredArgument]
        public InArgument<string> BookmarkName { get; set; }

        #endregion

        #region Properties

        protected override bool CanInduceIdle
        {
            get { return true; }
        }

        private BookmarkCallback BookmarkCallback
        {
            get
            {
                return this.bookmarkCallback ?? 
                      (this.bookmarkCallback = (activityContext, bookmark, value) =>
                                                activityContext.ResumeBookmark(new Bookmark(this.BookmarkName.Get(activityContext)), null));
            }
        }

        #endregion

        #region Methods

        protected override void Execute(NativeActivityContext context)
        {
            context.CreateBookmark(this.BookmarkName.Get(context), this.BookmarkCallback);
        }

        #endregion
    }

    public class WaitForBookmark<TResult> : NativeActivity<TResult> where TResult : class
    {
        #region Constants and Fields

        private BookmarkCallback bookmarkCallback;

        #endregion

        #region Public Properties

        [RequiredArgument]
        public InArgument<string> BookmarkName { get; set; }

        #endregion

        #region Properties

        protected override bool CanInduceIdle
        {
            get { return true; }
        }

        private BookmarkCallback BookmarkCallback
        {
            get
            {
                return this.bookmarkCallback ?? (this.bookmarkCallback = this.EndExecute);
            }
        }

        #endregion

        #region Methods

        protected override void Execute(NativeActivityContext context)
        {
            context.CreateBookmark(this.BookmarkName.Get(context), this.BookmarkCallback);
        }

        protected virtual void EndExecute(NativeActivityContext context, Bookmark bookmark, object value)
        {
            if (value is TResult)
            {
                this.Result.Set(context, value as TResult);
            }
            else if (value != null)
            {
                throw new InvalidOperationException("You must resume bookmark " + this.BookmarkName.Get(context)  + " with a result of type " + typeof(TResult).Name);
            }
        }

        #endregion
    }
}
