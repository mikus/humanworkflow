﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;

namespace CommonActivityLibrary
{

    public sealed class Notify : CodeActivity
    {
        public InArgument<string> To { get; set; }
        public InArgument<string> Text { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            string to = context.GetValue(this.To);
            string text = context.GetValue(this.Text);
            Console.WriteLine("Notifying " + to + " with text \"" + text + "\"");
        }
    }
}
