﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core;

namespace CommonActivityLibrary
{
    public class UserChooseResult
    {
        public User Invoker { get; set; }

        public string ChosenOption { get; set; }

        public DateTime Timestamp { get; set; }


        public UserChooseResult()
        {
            this.Timestamp = DateTime.Now;
        }
        
        public UserChooseResult(User Invoker, string ChosenOption) : this()
        {
            this.Invoker = Invoker;
            this.ChosenOption = ChosenOption;
        }
    }
}
